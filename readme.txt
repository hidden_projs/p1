To specify input file, go to Main.java and change variable inputFile.

Choices:

dragon.t
hilbert.t
triangle.t
mathtest.t

output is sent to the console

to create a .ps file please copy the code from the console without 
parser and lexer success messages and use any simple text editor to paste
the code, save the file under .ps


ERROR codes for parser unexpected tokens.

PROG = 400, PROCS = 401, PROC = 402, STMTS = 403,
STMT = 404, EXPR = 405, CONDITION = 406, TERM = 407, FACTOR = 408, EOF = 409, ACCEPT = 410, R = 411, T = 412

PROC = 256, FORWARD = 257, LEFT = 258, RIGHT = 259,
IF = 260, THEN = 261, ELSE = 262, ENDIF = 263, NUM = 264,
IDENT = 265, PLUS = 267, MINUS = 268, TIMES = 269, DIVIDE = 270,
LT = 271, LE = 272, EQ = 273, GT = 274, GE = 275, NE = 276, 
LP = 277, RP=278, TEMP = 279, CONDOP =280

FW_STMT = 700, IF_STMT = 701, LT_STMT = 702,
RT_STMT = 703, PROC_CALL = 704;

Please read report/report.pdf for further info.