package parser;

public class Action {
	// true = shift // false = reduce
	public static final int SHIFT = 0, REDUCE = 1, STAY = 2, ACCEPT = 3; 
	private int shiftReduceStay;
	private int reduceAction;
	private int nextState;
	
	public Action(int parserAction, int nextState, int reduceAction) {
		this.reduceAction = reduceAction;
		this.nextState = nextState;
		this.shiftReduceStay = parserAction;
	}

	public int getParserAction() {
		return shiftReduceStay;
	}

	public int getActionNum() {
		return reduceAction;
	}

	public int getNextState() {
		return nextState;
	}
	
	@Override
	public String toString(){
		return "red action " + reduceAction + " nextstate " + nextState + " parserAct " + shiftReduceStay;
	}
}
