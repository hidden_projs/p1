package parser;

import interCode.Condition;
import interCode.Expr;
import interCode.Factor;
import interCode.GrammarTree;
import interCode.Proc;
import interCode.Procs;
import interCode.Prog;
import interCode.R;
import interCode.Stmt;
import interCode.Stmts;
import interCode.T;
import interCode.Term;
import interCode_Stmt.FW_Stmt;
import interCode_Stmt.IF_Stmt;
import interCode_Stmt.LT_Stmt;
import interCode_Stmt.Proc_Call;
import interCode_Stmt.RT_Stmt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import tokens.Ident;
import tokens.Num;
import tokens.Tag;
import tokens.Token;

public class Parser {
	private ArrayList<Token> tokenList;
	private final HashMap<Integer, HashMap<Integer, Action>> stateTransition;;
	private HashMap<Integer, Action> transValNewState;
	private Stack<GrammarTree> parseStack;
	private Stack<Integer> stateStack;
	private GrammarTree transferToken;
	private Action currAction;

	public Parser(ArrayList<Token> tokenList) {
		this.tokenList = tokenList;
		tokenList.add(new Token(CodeTag.EOF, 0));
		ParserDFA dfa = new ParserDFA();
		stateTransition = dfa.getDFA();
		parseStack = new Stack<GrammarTree>();
		currAction = new Action(Action.STAY, -1, -1);
	}

	public void parse() {
		int currState = 0;

		int tokenCounter = 0;
		transferToken = tokenList.get(tokenCounter);
		stateStack = new Stack<Integer>();
		int tokenTag;

		while (true) {
			transValNewState = new HashMap<Integer, Action>(stateTransition.get(currState));
			tokenTag = transferToken.tag;
			// for all STMTs
			if (tokenTag >= 700 && tokenTag <= 704)
				tokenTag = CodeTag.STMT;
			// for cond operators
			if (tokenTag >= 271 && tokenTag <= 276)
				tokenTag = Tag.CONDOP;
			currAction = transValNewState.get(tokenTag);

			if (currAction == null) {
				System.out.println("Your code is syntatically incorrect.");
				System.out.println("Parser quit parsing");
				System.out.println("Reason: ");
				System.out.println("Unexpected token. Token code: " + transferToken.tag);
				try {
					System.out.println("At line: " + ((Token) transferToken).getLineNumber());
				} catch (Exception e) {
					// cannot displat line number
				}
				System.exit(1);	
			}
			
			
			if (currAction.getParserAction() == Action.STAY) {
				currState = currAction.getNextState();
				stateStack.push(currState);
				transferToken = tokenList.get(tokenCounter);
			} else if (currAction.getParserAction() == Action.SHIFT) {
				currState = currAction.getNextState();
				parseStack.push(transferToken);
				stateStack.push(currState);
				tokenCounter++;
				transferToken = tokenList.get(tokenCounter);
			} else if (currAction.getParserAction() == Action.REDUCE) {
				makeReduction(currAction.getActionNum());

				transferToken = parseStack.peek();

				if (stateStack.empty()) {
					currState = 0;
				} else {
					currState = stateStack.peek();
				}
				transferToken = parseStack.peek();
			} else if (currAction.getParserAction() == Action.ACCEPT) {
				System.out.println("SUCCESS PARSING");
				System.out.println();
				System.out.println();
				System.out.println();
				break;
			} else {
				// this else never happens
				// TODO error token, line
				System.out.println("FAIL");
				break;
			}
		}
	}

	private void makeReduction(int redAction) {
		switch (redAction) {
		case 0:
			// 0) prog� -> prog
			// not needed here, accept state is one before and it keeps prog on stack to continue working with aquired data
			break;
		case 1:
			// 1) prog -> procs

			stateStack.pop();
			parseStack.push(new Prog((Procs) parseStack.pop()));
			
			break;
		case 2:
			// 2) procs -> proc procs
			// top->bottom of stack: procs proc
			Procs procs = (Procs) parseStack.pop();
			procs.addProcedure((Proc) parseStack.pop());
			popStateStack(2);
			parseStack.push(new Procs(procs));
			break;
		case 3:
			// 3) procs -> empty
			parseStack.push(new Procs());
			break;
		case 4:
			// 4) proc -> "PROC" ident '(' ident ')' stmts
			// top->bottom of stack: stmts ) ident ( ident PROC
			// getting stmts
			Proc proc = new Proc((Stmts) parseStack.pop());
			// discarding bracket
			parseStack.pop();
			// setting proc identifier
			proc.setParametrID((Ident) parseStack.pop());
			// discarding bracket
			parseStack.pop();
			// setting parameter name
			proc.setProcIdent((Ident) parseStack.pop());
			// discarding PROC lexeme
			parseStack.pop();

			// getting to old state to continue parsing
			popStateStack(6);

			parseStack.push(new Proc(proc));
			break;
		case 5:
			// 5) stmts -> stmt stmts
			Stmts stmts = (Stmts) parseStack.pop();
			stmts.addStatement((Stmt) parseStack.pop());
			popStateStack(2);
			parseStack.push(new Stmts(stmts));
			break;
		case 6:
			// 6) stmts -> empty
			parseStack.push(new Stmts());
			break;
		case 7:
			// 7) stmt -> "FORWARD" expr
			popStateStack(2);
			Expr expr = (Expr) parseStack.pop();
			// discarding FORWARD lexeme
			parseStack.pop();
			parseStack.push(new FW_Stmt(new Expr(expr)));
			break;
		case 8:
			// 8) stmt -> "LEFT" expr
			popStateStack(2);
			Expr expr2 = (Expr) parseStack.pop();
			// discarding LEFT lexeme
			parseStack.pop();
			parseStack.push(new LT_Stmt(new Expr(expr2)));
			break;
		case 9:
			// 9) stmt -> "RIGHT" expr
			popStateStack(2);
			Expr expr3 = (Expr) parseStack.pop();
			// discarding RIGHT lexeme
			parseStack.pop();
			parseStack.push(new RT_Stmt(new Expr(expr3)));
			break;
		case 10:
			// 10) stmt -> ident '(' expr ')'

			// removing bracket
			parseStack.pop();

			Proc_Call procCall = new Proc_Call((Expr) parseStack.pop());
			// removing bracket
			parseStack.pop();
			
			procCall.setIdent((Ident) parseStack.pop());

			popStateStack(4);
			parseStack.push(new Proc_Call(procCall));
			break;
		case 11:
			// 11) stmt -> "IF" condition "THEN" stmts "ELSE" stmts "ENDIF"
			
			// dispose ENDIF lexeme
			parseStack.pop();
			// true stmts
			IF_Stmt ifStmt = new IF_Stmt((Stmts) parseStack.pop());
			// dispose ELSE lexeme
			parseStack.pop();
			// false stmts
			ifStmt.setTrueStmts((Stmts)parseStack.pop());
			// dispose THEN lexeme
			parseStack.pop();
			// condition
			ifStmt.setCondition( (Condition)parseStack.pop());
			// dispose IF lexeme
			parseStack.pop();

			popStateStack(7);

			parseStack.push(new IF_Stmt(ifStmt));
			break;
		case 12:
			// 12) expr -> term R
			popStateStack(2);
			parseStack.push(new Expr((R) parseStack.pop(), (Term) parseStack.pop()));
			break;
		case 13:
			// 13) condition -> expr conditionOp expr

			popStateStack(3);
			parseStack.push(new Condition((Expr) parseStack.pop(),
					(Token) parseStack.pop(), (Expr) parseStack.pop()));
			break;

		case 14:
			// 14) term -> factor T
			
			popStateStack(2);
			parseStack.push(new Term((T)parseStack.pop(),(Factor)parseStack.pop()));
			break;
		case 15:
			// 15) factor -> ident
			stateStack.pop();
			parseStack.push(new Factor((Ident)parseStack.pop()));
			break;
		case 16:
			// 16) factor -> num
			stateStack.pop();
			parseStack.push(new Factor((Num)parseStack.pop()));
			break;
		case 17:
			// 17) R -> empty
			parseStack.push(new R());
			break;
		case 18:
			// 18) R -> - term R
			popStateStack(3);
			parseStack.push(new R((R)parseStack.pop(),(Term)parseStack.pop(),(Token)parseStack.pop()));
			break;
		case 19:
			// 19) R -> + term R
			popStateStack(3);
			parseStack.push(new R((R)parseStack.pop(),(Term)parseStack.pop(),(Token)parseStack.pop()));
			break;
		case 20:
			// 20) T -> empty
			parseStack.push(new T());
			break;
		case 21:
			// 21) T -> * factor T
			popStateStack(3);
			parseStack.push(new T((T)parseStack.pop(),(Factor)parseStack.pop(),(Token)parseStack.pop()));
			break;
		case 22:
			// 22) T -> / factor T
			popStateStack(3);
			parseStack.push(new T((T)parseStack.pop(),(Factor)parseStack.pop(),(Token)parseStack.pop()));
			break;

		default:
			break;
		}
	}

	private void popStateStack(int j) {
		for (int i = 0; i < j; i++) {
			stateStack.pop();
		}
	}

	// reduction actions
	// 0) prog� -> prog
	// 1) prog -> procs
	// 2) procs -> proc procs
	// 3) procs -> empty ---------------
	// 4) proc -> "PROC" ident '(' ident ')' stmts
	// 5) stmts -> stmt stmts
	// 6) stmts -> empty ---------------
	// 7) stmt -> "FORWARD" expr
	// 8) stmt -> "LEFT" expr
	// 9) stmt -> "RIGHT" expr
	// 10) stmt -> ident '(' expr ')'
	// 11) stmt -> "IF" condition "THEN" stmts "ELSE" stmts "ENDIF"
	// 12) expr -> term R
	// 13) condition -> expr conditionOp expr
	// 14) term -> factor T
	// 15) factor -> ident
	// 16) factor -> num
	// 17) R -> empty ---------------
	// 18) R -> - term R
	// 19) R -> + term R
	// 20) T -> empty ---------------
	// 21) T -> * factor T
	// 22) T -> / factor T

	public Prog getParsedProg() {
		return (Prog) parseStack.pop();
	}

}
