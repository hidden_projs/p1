package parser;

import java.util.HashMap;

import tokens.Tag;

public class ParserDFA {
	
	private HashMap<Integer, HashMap<Integer, Action>> stateTransition;;
	private HashMap<Integer, Action> transValNewState;
	
	public ParserDFA() {
		transValNewState = new HashMap<Integer, Action>();
		stateTransition = new HashMap<Integer, HashMap<Integer, Action>>();
		
		transValNewState.put(Tag.PROC, new Action(Action.SHIFT, 4, -1));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 3));
		transValNewState.put(CodeTag.PROG, new Action(Action.STAY, 1, -1));
		transValNewState.put(CodeTag.PROCS, new Action(Action.STAY, 2, -1));
		transValNewState.put(CodeTag.PROC, new Action(Action.STAY, 3, -1));
		stateTransition.put(0, new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(CodeTag.EOF, new Action(Action.ACCEPT, -1, -1));
		stateTransition.put(1,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 1));
		stateTransition.put(2,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.PROC, new Action(Action.SHIFT, 4, -1));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 3));
		transValNewState.put(CodeTag.PROCS, new Action(Action.STAY, 5, -1));
		transValNewState.put(CodeTag.PROC, new Action(Action.STAY, 3, -1));
		stateTransition.put(3,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.IDENT, new Action(Action.SHIFT, 6, -1));
		stateTransition.put(4,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 2));
		stateTransition.put(5,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.LP, new Action(Action.SHIFT, 7, -1));
		stateTransition.put(6,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.IDENT, new Action(Action.SHIFT, 8, -1));
		stateTransition.put(7,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.RP, new Action(Action.SHIFT, 9, -1));
		stateTransition.put(8,  new HashMap(transValNewState));
		transValNewState.clear();
	
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 6));
		transValNewState.put(Tag.FORWARD, new Action(Action.SHIFT, 12, -1));
		transValNewState.put(Tag.LEFT, new Action(Action.SHIFT, 13, -1));
		transValNewState.put(Tag.RIGHT, new Action(Action.SHIFT, 14, -1));
		transValNewState.put(Tag.IF, new Action(Action.SHIFT, 15, -1));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 6));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 6));
		transValNewState.put(Tag.IDENT, new Action(Action.SHIFT, 16, -1));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 6));
		transValNewState.put(CodeTag.STMTS, new Action(Action.STAY, 10, -1));
		transValNewState.put(CodeTag.STMT, new Action(Action.STAY, 11, -1));
		stateTransition.put(9,  new HashMap(transValNewState));
		transValNewState.clear();
	
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 4));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 4));
		stateTransition.put(10,  new HashMap(transValNewState));
		transValNewState.clear();
	
		
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 6));
		transValNewState.put(Tag.FORWARD, new Action(Action.SHIFT, 12, -1));
		transValNewState.put(Tag.LEFT, new Action(Action.SHIFT, 13, -1));
		transValNewState.put(Tag.RIGHT, new Action(Action.SHIFT, 14, -1));
		transValNewState.put(Tag.IF, new Action(Action.SHIFT, 15, -1));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 6));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 6));
		transValNewState.put(Tag.IDENT, new Action(Action.SHIFT, 16, -1));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 6));
		transValNewState.put(CodeTag.STMTS, new Action(Action.STAY, 17, -1));
		transValNewState.put(CodeTag.STMT, new Action(Action.STAY, 11, -1));
		stateTransition.put(11,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.NUM, new Action(Action.SHIFT, 21, -1));
		transValNewState.put(Tag.IDENT, new Action(Action.SHIFT, 22, -1));
		transValNewState.put(CodeTag.EXPR, new Action(Action.STAY, 18, -1));
		transValNewState.put(CodeTag.TERM, new Action(Action.STAY, 19, -1));
		transValNewState.put(CodeTag.FACTOR, new Action(Action.STAY, 20, -1));
		stateTransition.put(12,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.NUM, new Action(Action.SHIFT, 21, -1));
		transValNewState.put(Tag.IDENT, new Action(Action.SHIFT, 22, -1));
		transValNewState.put(CodeTag.EXPR, new Action(Action.STAY, 23, -1));
		transValNewState.put(CodeTag.TERM, new Action(Action.STAY, 19, -1));
		transValNewState.put(CodeTag.FACTOR, new Action(Action.STAY, 20, -1));
		stateTransition.put(13,  new HashMap(transValNewState));
		transValNewState.clear();
	
		transValNewState.put(Tag.NUM, new Action(Action.SHIFT, 21, -1));
		transValNewState.put(Tag.IDENT, new Action(Action.SHIFT, 22, -1));
		transValNewState.put(CodeTag.EXPR, new Action(Action.STAY, 24, -1));
		transValNewState.put(CodeTag.TERM, new Action(Action.STAY, 19, -1));
		transValNewState.put(CodeTag.FACTOR, new Action(Action.STAY, 20, -1));
		stateTransition.put(14,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.NUM, new Action(Action.SHIFT, 21, -1));
		transValNewState.put(Tag.IDENT, new Action(Action.SHIFT, 22, -1));
		transValNewState.put(CodeTag.EXPR, new Action(Action.STAY, 25, -1));
		transValNewState.put(CodeTag.CONDITION, new Action(Action.STAY, 26, -1));
		transValNewState.put(CodeTag.TERM, new Action(Action.STAY, 19, -1));
		transValNewState.put(CodeTag.FACTOR, new Action(Action.STAY, 20, -1));
		stateTransition.put(15,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.LP, new Action(Action.SHIFT, 27, -1));
		stateTransition.put(16,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 5));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 5));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 5));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 5));
		stateTransition.put(17,  new HashMap(transValNewState));
		transValNewState.clear();
	
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 7));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 7));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 7));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 7));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 7));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 7));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 7));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 7));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 7));
		stateTransition.put(18,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.RP, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.PLUS, new Action(Action.SHIFT, 28, -1));
		transValNewState.put(Tag.MINUS, new Action(Action.SHIFT, 29, -1));
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.THEN, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.CONDOP, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(CodeTag.R, new Action(Action.STAY, 30, -1));
		stateTransition.put(19,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.RP, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.TIMES, new Action(Action.SHIFT, 31, -1));
		transValNewState.put(Tag.PLUS, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.MINUS, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.DIVIDE, new Action(Action.SHIFT, 32, -1));
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.THEN, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.CONDOP, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(CodeTag.T, new Action(Action.STAY, 33, -1));
		stateTransition.put(20,  new HashMap(transValNewState));
		transValNewState.clear();
	
		transValNewState.put(Tag.RP, new Action(Action.REDUCE, -1, 16));
		transValNewState.put(Tag.TIMES, new Action(Action.REDUCE, -1, 16));
		transValNewState.put(Tag.PLUS, new Action(Action.REDUCE, -1, 16));
		transValNewState.put(Tag.MINUS, new Action(Action.REDUCE, -1, 16));
		transValNewState.put(Tag.DIVIDE, new Action(Action.REDUCE, -1, -16));
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 16));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 16));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 16));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 16));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 16));
		transValNewState.put(Tag.THEN, new Action(Action.REDUCE, -1, 16));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 16));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 16));
		transValNewState.put(Tag.CONDOP, new Action(Action.REDUCE, -1, 16));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 16));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 16));
		stateTransition.put(21,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.RP, new Action(Action.REDUCE, -1, 15));
		transValNewState.put(Tag.TIMES, new Action(Action.REDUCE, -1, 15));
		transValNewState.put(Tag.PLUS, new Action(Action.REDUCE, -1, 15));
		transValNewState.put(Tag.MINUS, new Action(Action.REDUCE, -1, 15));
		transValNewState.put(Tag.DIVIDE, new Action(Action.REDUCE, -1, -15));
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 15));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 15));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 15));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 15));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 15));
		transValNewState.put(Tag.THEN, new Action(Action.REDUCE, -1, 15));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 15));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 15));
		transValNewState.put(Tag.CONDOP, new Action(Action.REDUCE, -1, 15));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 15));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 15));
		stateTransition.put(22,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 8));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 8));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 8));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 8));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 8));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 8));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 8));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 8));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 8));
		stateTransition.put(23,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 9));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 9));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 9));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 9));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 9));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 9));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 9));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 9));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 9));
		stateTransition.put(24,  new HashMap(transValNewState));
		transValNewState.clear();
	
		transValNewState.put(Tag.CONDOP, new Action(Action.SHIFT, 34, -1));
		stateTransition.put(25,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.THEN, new Action(Action.SHIFT, 35, -1));
		stateTransition.put(26,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.NUM, new Action(Action.SHIFT, 21, -1));
		transValNewState.put(Tag.IDENT, new Action(Action.SHIFT, 22, -1));
		transValNewState.put(CodeTag.EXPR, new Action(Action.STAY, 36, -1));
		transValNewState.put(CodeTag.TERM, new Action(Action.STAY, 19, -1));
		transValNewState.put(CodeTag.FACTOR, new Action(Action.STAY, 20, -1));
		stateTransition.put(27,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.NUM, new Action(Action.SHIFT, 21, -1));
		transValNewState.put(Tag.IDENT, new Action(Action.SHIFT, 22, -1));
		transValNewState.put(CodeTag.TERM, new Action(Action.STAY, 37, -1));
		transValNewState.put(CodeTag.FACTOR, new Action(Action.STAY, 20, -1));
		stateTransition.put(28,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.NUM, new Action(Action.SHIFT, 21, -1));
		transValNewState.put(Tag.IDENT, new Action(Action.SHIFT, 22, -1));
		transValNewState.put(CodeTag.TERM, new Action(Action.STAY, 38, -1));
		transValNewState.put(CodeTag.FACTOR, new Action(Action.STAY, 20, -1));
		stateTransition.put(29,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.RP, new Action(Action.REDUCE, -1, 12));
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 12));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 12));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 12));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 12));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 12));
		transValNewState.put(Tag.THEN, new Action(Action.REDUCE, -1, 12));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 12));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 12));
		transValNewState.put(Tag.CONDOP, new Action(Action.REDUCE, -1, 12));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 12));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 12));
		stateTransition.put(30,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.NUM, new Action(Action.SHIFT, 21, -1));
		transValNewState.put(Tag.IDENT, new Action(Action.SHIFT, 22, -1));
		transValNewState.put(CodeTag.FACTOR, new Action(Action.STAY, 39, -1));
		stateTransition.put(31,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.NUM, new Action(Action.SHIFT, 21, -1));
		transValNewState.put(Tag.IDENT, new Action(Action.SHIFT, 22, -1));
		transValNewState.put(CodeTag.FACTOR, new Action(Action.STAY, 40, -1));
		stateTransition.put(32,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.RP, new Action(Action.REDUCE, -1, 14));
		transValNewState.put(Tag.PLUS, new Action(Action.REDUCE, -1, 14));
		transValNewState.put(Tag.MINUS, new Action(Action.REDUCE, -1, 14));
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 14));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 14));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 14));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 14));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 14));
		transValNewState.put(Tag.THEN, new Action(Action.REDUCE, -1, 14));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 14));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 14));
		transValNewState.put(Tag.CONDOP, new Action(Action.REDUCE, -1, 14));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 14));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 14));
		stateTransition.put(33,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.NUM, new Action(Action.SHIFT, 21, -1));
		transValNewState.put(Tag.IDENT, new Action(Action.SHIFT, 22, -1));
		transValNewState.put(CodeTag.EXPR, new Action(Action.STAY, 41, -1));
		transValNewState.put(CodeTag.TERM, new Action(Action.STAY, 19, -1));
		transValNewState.put(CodeTag.FACTOR, new Action(Action.STAY, 20, -1));
		stateTransition.put(34,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 6));
		transValNewState.put(Tag.FORWARD, new Action(Action.SHIFT, 12, -1));
		transValNewState.put(Tag.LEFT, new Action(Action.SHIFT, 13, -1));
		transValNewState.put(Tag.RIGHT, new Action(Action.SHIFT, 14, -1));
		transValNewState.put(Tag.IF, new Action(Action.SHIFT, 15, -1));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 6));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 6));
		transValNewState.put(Tag.IDENT, new Action(Action.SHIFT, 16, -1));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 6));
		transValNewState.put(CodeTag.STMTS, new Action(Action.STAY, 42, -1));
		transValNewState.put(CodeTag.STMT, new Action(Action.STAY, 11, -1));
		stateTransition.put(35,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.RP, new Action(Action.SHIFT, 43, -1));
		stateTransition.put(36,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.RP, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.PLUS, new Action(Action.SHIFT, 28, -1));
		transValNewState.put(Tag.MINUS, new Action(Action.SHIFT, 29, -1));
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.THEN, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.CONDOP, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(CodeTag.R, new Action(Action.STAY, 44, -1));
		stateTransition.put(37,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.RP, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.PLUS, new Action(Action.SHIFT, 28, -1));
		transValNewState.put(Tag.MINUS, new Action(Action.SHIFT, 29, -1));
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.THEN, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.CONDOP, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 17));
		transValNewState.put(CodeTag.R, new Action(Action.STAY, 45, -1));
		stateTransition.put(38,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.RP, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.TIMES, new Action(Action.SHIFT, 31, -1));
		transValNewState.put(Tag.PLUS, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.MINUS, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.DIVIDE, new Action(Action.SHIFT, 32, -1));
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.THEN, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.CONDOP, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(CodeTag.T, new Action(Action.STAY, 46, -1));
		stateTransition.put(39,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.RP, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.TIMES, new Action(Action.SHIFT, 31, -1));
		transValNewState.put(Tag.PLUS, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.MINUS, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.DIVIDE, new Action(Action.SHIFT, 32, -1));
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.THEN, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.CONDOP, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 20));
		transValNewState.put(CodeTag.T, new Action(Action.STAY, 47, -1));
		stateTransition.put(40,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.THEN, new Action(Action.REDUCE, -1, 13));
		stateTransition.put(41,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.ELSE, new Action(Action.SHIFT, 48, 13));
		stateTransition.put(42,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 10));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 10));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 10));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 10));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 10));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 10));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 10));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 10));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 10));
		stateTransition.put(43,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.RP, new Action(Action.REDUCE, -1, 18));
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 18));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 18));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 18));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 18));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 18));
		transValNewState.put(Tag.THEN, new Action(Action.REDUCE, -1, 18));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 18));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 18));
		transValNewState.put(Tag.CONDOP, new Action(Action.REDUCE, -1, 18));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 18));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 18));
		stateTransition.put(44,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.RP, new Action(Action.REDUCE, -1, 19));
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 19));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 19));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 19));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 19));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 19));
		transValNewState.put(Tag.THEN, new Action(Action.REDUCE, -1, 19));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 19));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 19));
		transValNewState.put(Tag.CONDOP, new Action(Action.REDUCE, -1, 19));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 19));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 19));
		stateTransition.put(45,  new HashMap(transValNewState));
		transValNewState.clear();

		transValNewState.put(Tag.RP, new Action(Action.REDUCE, -1, 21));
		transValNewState.put(Tag.PLUS, new Action(Action.REDUCE, -1, 21));
		transValNewState.put(Tag.MINUS, new Action(Action.REDUCE, -1, 21));
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 21));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 21));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 21));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 21));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 21));
		transValNewState.put(Tag.THEN, new Action(Action.REDUCE, -1, 21));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 21));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 21));
		transValNewState.put(Tag.CONDOP, new Action(Action.REDUCE, -1, 21));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 21));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 21));
		stateTransition.put(46,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.RP, new Action(Action.REDUCE, -1, 22));
		transValNewState.put(Tag.PLUS, new Action(Action.REDUCE, -1, 22));
		transValNewState.put(Tag.MINUS, new Action(Action.REDUCE, -1, 22));
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 22));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 22));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 22));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 22));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 22));
		transValNewState.put(Tag.THEN, new Action(Action.REDUCE, -1, 22));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 22));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 22));
		transValNewState.put(Tag.CONDOP, new Action(Action.REDUCE, -1, 22));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 22));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 22));
		stateTransition.put(47,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 6));
		transValNewState.put(Tag.FORWARD, new Action(Action.SHIFT, 12, -1));
		transValNewState.put(Tag.LEFT, new Action(Action.SHIFT, 13, -1));
		transValNewState.put(Tag.RIGHT, new Action(Action.SHIFT, 14, -1));
		transValNewState.put(Tag.IF, new Action(Action.SHIFT, 15, -1));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 6));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 6));
		transValNewState.put(Tag.IDENT, new Action(Action.SHIFT, 16, -1));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 6));
		transValNewState.put(CodeTag.STMTS, new Action(Action.STAY, 49, -1));
		transValNewState.put(CodeTag.STMT, new Action(Action.STAY, 11, -1));
		stateTransition.put(48,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.ENDIF, new Action(Action.SHIFT, 50, -1));
		stateTransition.put(49,  new HashMap(transValNewState));
		transValNewState.clear();
		
		transValNewState.put(Tag.PROC, new Action(Action.REDUCE, -1, 11));
		transValNewState.put(Tag.FORWARD, new Action(Action.REDUCE, -1, 11));
		transValNewState.put(Tag.LEFT, new Action(Action.REDUCE, -1, 11));
		transValNewState.put(Tag.RIGHT, new Action(Action.REDUCE, -1, 11));
		transValNewState.put(Tag.IF, new Action(Action.REDUCE, -1, 11));
		transValNewState.put(Tag.ELSE, new Action(Action.REDUCE, -1, 11));
		transValNewState.put(Tag.ENDIF, new Action(Action.REDUCE, -1, 11));
		transValNewState.put(Tag.IDENT, new Action(Action.REDUCE, -1, 11));
		transValNewState.put(CodeTag.EOF, new Action(Action.REDUCE, -1, 11));
		stateTransition.put(50,  new HashMap(transValNewState));
		transValNewState.clear();
	}
	
	public HashMap<Integer, HashMap<Integer, Action>> getDFA() {
		return stateTransition;
	}
}
