package interCode_Stmt;

import interCode.Condition;
import interCode.Expr;
import interCode.Stmt;
import interCode.Stmts;

public class IF_Stmt extends Stmt {
	private Condition condition;
	private Stmts trueStmts;
	private Stmts falseStmts;

	public IF_Stmt(Stmts falseStmts) {
		super(StmtTag.IF_STMT);
		this.falseStmts = falseStmts;
	}

	public IF_Stmt(IF_Stmt ifStmt) {
		super(StmtTag.IF_STMT);
		condition = ifStmt.getCondition();
		trueStmts = ifStmt.getTrueStmts();
		falseStmts = ifStmt.getFalseStmts();
	}

	public void setTrueStmts(Stmts trueStmts) {
		this.trueStmts = trueStmts;
	}
	
	public void setCondition(Condition cond) {
		condition = cond;
	}


	public Condition getCondition() {
		return condition;
	}

	public Stmts getTrueStmts() {
		return trueStmts;
	}

	public Stmts getFalseStmts() {
		return falseStmts;
	}

	@Override
	public void traverseTree(){
		// condition {exp1} {exp2} ifelse
		
		condition.traverseTree();
		super.printCode("{");		
		trueStmts.traverseTree();
		super.printCode("} {");
		falseStmts.traverseTree();
		super.printCode(" } ifelse");
	}
	
	@Override
	public void cleanUpNullVals() {
		trueStmts.cleanUpNullVals();
		falseStmts.cleanUpNullVals();
		condition.cleanUpNullVals();
	}


}
