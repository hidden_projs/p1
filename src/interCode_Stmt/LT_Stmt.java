package interCode_Stmt;

import tokens.Tag;
import interCode.Expr;
import interCode.Stmt;

public class LT_Stmt extends Stmt {
	private Expr expression;
	
	public LT_Stmt(Expr expr) {
		super(StmtTag.LT_STMT);
		expression = expr;
	}

	
	@Override
	public void cleanUpNullVals() {
		expression.cleanUpNullVals();
	}
	
	public Expr getExpression(){
		return expression;
	}
	
	@Override
	public void traverseTree(){
		expression.traverseTree();
		super.printCode("Left");
	}
}
