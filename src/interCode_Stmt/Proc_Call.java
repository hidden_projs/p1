package interCode_Stmt;

import tokens.Ident;
import interCode.Expr;
import interCode.Stmt;

public class Proc_Call extends Stmt {
	private Ident callId;
	private Expr callParams;
	
	public Proc_Call(Expr expr) {
		super(StmtTag.PROC_CALL);
		callParams = expr;
	}

	public Proc_Call(Proc_Call procCall) {
		super(StmtTag.PROC_CALL);
		callId = procCall.getCallId();
		callParams = procCall.getCallParams();
	}

	public void setIdent(Ident callId) {
		this.callId = callId;
	}

	public Expr getCallParams() {
		return callParams;
	}

	public Ident getCallId() {
		return callId;
	}
	
	@Override
	public void cleanUpNullVals() {
		callParams.cleanUpNullVals();
	}
	
	public Expr getExpression(){
		return callParams;
	}
	
	@Override
	public void traverseTree(){
		callParams.traverseTree();
		callId.traverseTree();
	}
}
