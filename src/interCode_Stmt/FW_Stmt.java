package interCode_Stmt;

import interCode.Expr;
import interCode.Proc;
import interCode.Stmt;

public class FW_Stmt extends Stmt{
	private Expr expression;
	
	public FW_Stmt(Expr expr) {
		super(StmtTag.FW_STMT);
		expression = expr;
	}

	@Override
	public void cleanUpNullVals() {
		expression.cleanUpNullVals();
	}
	
	@Override
	public void traverseTree(){
		expression.traverseTree();
		super.printCode("Forward");
	}
	
	public Expr getExpression(){
		return expression;
	}
	
	
}
