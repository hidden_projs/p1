package interCode_Stmt;

import tokens.Tag;
import interCode.Expr;
import interCode.Stmt;

public class RT_Stmt extends Stmt {
	private Expr expression;
	
	public RT_Stmt(Expr expr) {
		super(StmtTag.RT_STMT);
		expression = expr;
	}

	@Override
	public void cleanUpNullVals() {
		expression.cleanUpNullVals();
	}
	
	public Expr getExpression(){
		return expression;
	}
	
	@Override
	public void traverseTree(){
		expression.traverseTree();
		super.printCode("Right");
	}
}
