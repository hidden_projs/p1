package tokens;

import interCode.GrammarTree;

public class Token extends GrammarTree {
	private int lineNumber;
	
	public Token(int t, int lineNumber) {
		super(t);
		this.lineNumber = lineNumber;
	}

	public String toString() {
		return tag + " line " + lineNumber;
	}
	
	public int getTag() {
		return tag;
	}

	public int getLineNumber() {
		return lineNumber;
	}	
}
