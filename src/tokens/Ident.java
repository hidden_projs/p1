package tokens;

public class Ident extends Token {	
	private String lexeme = "";

	public Ident(String s, int tag, int line) { 
			super(tag, line); lexeme = s; 
		}

	public String toString() {
		return lexeme + " tag " + tag + " line " + getLineNumber();
	}
	
	@Override
	public void traverseTree(){
		printCode(lexeme);
	}
	
	public String getLexeme() {
		return lexeme;
	}
}
