package tokens;

public class Num extends Token{
	
	private final int value;
	
	public Num(int value, int line) {
		super(Tag.NUM, line);
		this.value = value;
	}

	public String toString() {
		return "NUM " + value + " line " + getLineNumber();
	}
	
	@Override
	public void traverseTree(){
		printCode(value + "");
	}
	
}
