package interCode;

import parser.CodeTag;
import tokens.Ident;
import tokens.Num;

public class Factor extends GrammarTree{
	private final Num number;
	private final Ident identifier;
	
	public Factor(Num num) {
		super(CodeTag.FACTOR);
		number = num;
		identifier = null;
	}
	
	public Factor(Ident ident) {
		super(CodeTag.FACTOR);
		identifier = ident;
		number = null;
	}

	
	public boolean isEmpty() {
		return false;
	}
	
	public Num getNum() {
		return number;
	}
	
	public Ident getIdent(){
		return identifier;
	}
	
	@Override
	public void traverseTree(){
		if (number== null) {
			identifier.traverseTree();
		} else {
			number.traverseTree();
		}
		
	}
}
