package interCode;

import parser.CodeTag;
import tokens.Ident;

public class Proc extends GrammarTree{
	private Stmts statements;
	private Ident procedureId;
	private Ident paramId;
	
	public Proc(Stmts stmts) {
		super(CodeTag.PROC);
		this.statements = stmts;
	}
	
	public Proc(Proc proc) {
		super(CodeTag.PROC);
		statements = proc.getStatements();
		procedureId = proc.getProcedureId();
		paramId = proc.getParamId();
	}
	
	public Stmts getStatements() {
		return statements;
	}

	public Ident getProcedureId() {
		return procedureId;
	}

	public Ident getParamId() {
		return paramId;
	}

	public void setProcIdent(Ident ident) {
		procedureId = ident;
	}

	public void setParametrID(Ident ident) {
		paramId = ident;
	}
	
	public boolean isEmpty() {
		return false;
	}
	
	@Override
	public void cleanUpNullVals() {
		statements.cleanUpNullVals();
	}
	
	@Override
	public void traverseTree() {
		// proc already printed in prog?
		super.printCode("/" + procedureId.getLexeme() + " { " );
		
		// MAin void has no params
		if (!procedureId.getLexeme().equals("MAIN")) {
			super.printCode("1 dict begin");
			super.printCode("/" + paramId.getLexeme() + " exch def" );
		}
		statements.traverseTree();
		
		if (!procedureId.getLexeme().equals("MAIN")) {
			super.printCode("end } def");
			//super.printCode("} def");
		} else {
			super.printCode("} def");
		}
		
	}
}
