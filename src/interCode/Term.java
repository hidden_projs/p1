package interCode;

import parser.CodeTag;

public class Term extends GrammarTree {
	private Factor factor;
	private T t;
	
	public Term(T t, Factor factor) {
		super(CodeTag.TERM);
		this.t =t;
		this.factor = factor;
	}
	
	@Override
	public boolean isEmpty() {
		// never empty
		return false;
	}
	
	@Override
	public void cleanUpNullVals() {
		if (t.isEmpty()) {
			t = null;
		} else {
			t.cleanUpNullVals();
		}
		// factor = num or ident
	}
	
	@Override
	public void traverseTree(){
		factor.traverseTree();
		if (t != null){
			t.traverseTree();
		}
	}
}
