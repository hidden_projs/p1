package interCode;

import parser.CodeTag;

public class Prog extends GrammarTree{
	
	private final Procs procedures;	
	
	public Prog(Procs procs) {
		super(CodeTag.PROG);
		this.procedures = procs;
	}
	
	public Procs getProcedures() {
		return procedures;
	}
	
	@Override
	public boolean isEmpty() {
		return false;
	}
	
	@Override
	public void cleanUpNullVals() {
		procedures.cleanUpNullVals();
	}
	
	@Override
	public void traverseTree() {
		procedures.traverseTree();
	}
	
	@Override
	public void createCode(){
		traverseTree();
	}
}
