package interCode;

import parser.CodeTag;
import tokens.Token;

public class Condition extends GrammarTree{
	private final Expr expression_1;
	private final Token condOperator;
	private final Expr expression_2;
	
	
	public Condition(Expr expr2, Token condOp, Expr expr1) {
		super(CodeTag.CONDITION);
		expression_1 = expr1;
		expression_2 = expr2;
		condOperator = condOp;
		
	}
	
	public boolean evaluateCondition(){
		//TODO
		return true;
	}
	
	public boolean isEmpty() {
		return false;
	}
	
	@Override
	public void cleanUpNullVals(){
		expression_1.cleanUpNullVals();
		expression_2.cleanUpNullVals();
	}
	
	@Override
	public void traverseTree(){
		expression_1.traverseTree();
		expression_2.traverseTree();
		super.printCode(GrammarTree.condOpMap.get(condOperator.getTag()));
	}
}
