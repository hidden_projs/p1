package interCode;

import interCode_Stmt.FW_Stmt;
import interCode_Stmt.IF_Stmt;
import interCode_Stmt.LT_Stmt;
import interCode_Stmt.Proc_Call;
import interCode_Stmt.RT_Stmt;

import java.util.ArrayList;

import parser.CodeTag;

public class Stmts extends GrammarTree {
	private ArrayList<Stmt> statements = new ArrayList<Stmt>();
	
	
	public Stmts(Stmts stmts) {
		super(CodeTag.STMTS);
		statements = stmts.getStmts();
	}
	
	public Stmts() {
		super(CodeTag.STMTS);
	}

	public void addStatement (Stmt stmt) {
		statements.add(stmt);
	}
	
	public ArrayList<Stmt> getStmts() {
		return statements;
	}	
	
	@Override
	public void traverseTree() {
		// print stmt, or they were printed earlier?
		// stmts are in reverse order
		for (int i = (statements.size()-1); i >= 0; i--) {
			statements.get(i).traverseTree();
		}
		System.out.println();
	}
	
	@Override
	public boolean isEmpty() {
		return false;
	}
	
	@Override
	public void cleanUpNullVals() {
		for (Stmt statement : statements) {
			if (statement instanceof FW_Stmt) {
				((FW_Stmt)statement).cleanUpNullVals();
			} else if (statement instanceof RT_Stmt) {
				((RT_Stmt)statement).cleanUpNullVals();
			} else if (statement instanceof IF_Stmt) {
				((IF_Stmt)statement).cleanUpNullVals();
			} else if (statement instanceof LT_Stmt) {
				((LT_Stmt)statement).cleanUpNullVals();
			} else if (statement instanceof Proc_Call) {
				((Proc_Call)statement).cleanUpNullVals();
			}
			
		}
	}
}
