package interCode;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;


public class GrammarTree {
	public final int tag;
	
	private Prog programm;
	
//	file creation code
//	file creation code	protected BufferedWriter fileWriter = null;
//	file creation code	protected File file;
	public static HashMap<Integer, String> condOpMap;
	public static HashMap<Integer, String> signMap;
	
	public GrammarTree(int t) {
		tag = t;
	}
	
	public void assignProg(Prog prog) {
		programm = prog;
	}
	
	public void createOutputFile(String fileName) {
//		file creation code
//		// create file
//		file = new File(fileName);
//		// create fileWriter
//		try {
//			fileWriter = new BufferedWriter(new FileWriter(fileName));
//		} catch (IOException e) {
//			System.out.println("IO Exception");
//		}
		
		
		condOpMap = new HashMap<Integer, String>();
		condOpMap.put(271, "lt");
		condOpMap.put(272, "le");
		condOpMap.put(273, "eq");
		condOpMap.put(274, "gt");
		condOpMap.put(275, "ge");
		condOpMap.put(276, "ne");

		signMap = new HashMap<Integer, String>();
		signMap.put(267, "add");
		signMap.put(268, "sub");
		signMap.put(269, "mul");
		signMap.put(270, "div");
		
		
	}
	
	public void printCode(String code) {
		System.out.println(code);
//		try {
//			fileWriter.write(code);
//			fileWriter.flush();
//		
//		} catch (NullPointerException e) {
//			System.out.println("Output file is not specified");
//			System.out.println(e.getMessage());
//		} catch (IOException e) {
//			System.out.println("Io Exception");
//			System.out.println(e.getMessage());
//		}
	}
	
	public void createCode(){
		traverseTree();
	}
	
	public boolean isEmpty() {
		return false;
	}
	
	public void cleanUpNullVals() {
		programm.cleanUpNullVals();
	}
	
	public void traverseTree() {
		addHeading();
		programm.traverseTree();
		addTail();
//		file creation code
//		try {
//			fileWriter.write();
//			fileWriter.flush();
//			fileWriter.close();
//		} catch (IOException e) {
//			System.out.println("io exception");
//		}
	}
	public void addHeading(){
		  System.out.println("%!PS-Adobe-3.0");	// Adobe header
		    /* rest of prologue ... */
		    System.out.println("/Xpos    { 300 } def");
		    System.out.println("/Ypos    { 500 } def");
		    System.out.println("/Heading { 0   } def");
		    System.out.println("/Arg     { 0   } def");
		    //Implementation of Right, Left and Forward procedures in PostScript
		    System.out.println("/Right   {");
		    System.out.println("Heading exch add Trueheading");
		    System.out.println("/Heading exch def");
		    System.out.println("} def");
		    System.out.println("/Left {");
		    System.out.println("Heading exch sub Trueheading");
		    System.out.println("/Heading exch def");
		    System.out.println("} def");
		    System.out.println("/Trueheading {");
		    System.out.println("360 mod dup");
		    System.out.println("0 lt { 360 add } if");
		    System.out.println("} def");
		    System.out.println("/Forward {");
		    System.out.println("dup  Heading sin mul");
		    System.out.println("exch Heading cos mul");
		    System.out.println("2 copy Newposition");
		    System.out.println("rlineto");
		    System.out.println("} def");
		    System.out.println("/Newposition {");
		    System.out.println("Heading 180 gt Heading 360 lt");
		    System.out.println("and { neg } if exch");
		    System.out.println("Heading  90 gt Heading 270 lt");
		    System.out.println("and { neg } if exch");
		    System.out.println("Ypos add /Ypos exch def");
		    System.out.println("Xpos add /Xpos exch def");
		    System.out.println("} def");
	  }

	  public void addTail() {
		  /* epilogue ... */
		    System.out.println("Xpos Ypos moveto");
		    //Call Main to start
		    System.out.println("MAIN");
		    System.out.println("stroke");
		    System.out.println("showpage");
	  }
}
