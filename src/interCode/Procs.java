package interCode;

import java.util.ArrayList;

import parser.CodeTag;

public class Procs extends GrammarTree{
	private ArrayList<Proc> procedures = new ArrayList<Proc>();
	private Proc procedureMain;

	public Procs(Procs procs) {
		super(CodeTag.PROCS);
		procedures = new ArrayList<Proc>(procs.getProcedures());
		
		// TODO Auto-generated constructor stub
	}
	
	public Procs() {
		super(CodeTag.PROCS);
		// TODO Auto-generated constructor stub
	}
	
	public void addProcedure(Proc proc) {
		procedures.add(proc);
	}
	
	public ArrayList<Proc> getProcedures() {
		return procedures;
	}
	
	@Override
	public void traverseTree() {
		// print proc
		// procs are in reverse order but it doesnt matter
		// start from MAIN??
		for (Proc procedure : procedures){
			if (procedure.getProcedureId().getLexeme().equals("MAIN")) {
				procedureMain = procedure;
			} else {
			procedure.traverseTree();
			}
		}
		procedureMain.traverseTree();
	}
	
	@Override
	public void cleanUpNullVals() {
		for (Proc procedure : procedures){
			procedure.cleanUpNullVals();
		}
	}
	
	public boolean isEmpty() {
		return false;
	}
	
}
