package interCode;

import parser.CodeTag;
import tokens.Token;

public class T extends GrammarTree {
	private Token sign;
	private Factor factor;
	private T t;


	public T(T t, Factor fac, Token sign) {
		super(CodeTag.T);
		this.sign = sign;
		this.factor = fac;
		this.t = t;
	}

	public T() {
		super(CodeTag.T);
		sign = null;
		factor = null;
		t = null;
	}
	
	
	public Token getSign() {
		return sign;
	}

	public Factor getFactor() {
		return factor;
	}

	public T getT() {
		return t;
	}
	
	@Override
	public void cleanUpNullVals() {
		if (t.isEmpty()) {
			t = null;
		} else {
			t.cleanUpNullVals();
		}
		// factor is num or ident
	}
	
	@Override
	public boolean isEmpty() {
		if (sign == null && factor == null && t == null)
			return true;
		return false;
	}
	
	@Override
	public void traverseTree(){
		factor.traverseTree();
		if (sign != null) {
			super.printCode(GrammarTree.signMap.get(sign.getTag()));
		}
		if (t != null){
			t.traverseTree();
		}
	}
	
}
