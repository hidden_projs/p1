package interCode;

import parser.CodeTag;


public class Expr extends GrammarTree{
	private Term term;
	private R r;

	public Expr(R r, Term term) {
		super(CodeTag.EXPR);
		this.term = term;
		this.r = r;
	}
	
	public Expr(Expr expr) {
		super(CodeTag.EXPR);
		this.term = expr.getTerm();
		this.r = expr.getR();
	}

	private Term getTerm() {
		return term;
	}

	private R getR() {
		return r;
	}
	
	public boolean isEmpty() {
		return false;
	}
	
	@Override
	public void cleanUpNullVals() {
		if (r.isEmpty()) {
			r = null;
		} else {
			r.cleanUpNullVals();
		}
		term.cleanUpNullVals();
	}
	
	@Override
	public void traverseTree(){
		term.traverseTree();
		if (r != null) {
			r.traverseTree();
		}
	}
}
