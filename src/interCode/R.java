package interCode;

import parser.CodeTag;
import tokens.Token;

public class R extends GrammarTree {
	private Token sign;
	private Term term;
	private R r;
	
	public R(R r, Term term, Token sign) {
		super(CodeTag.R);
		this.sign = sign;
		this.term = term;
		this.r = r;
	}
	
	public R(){
		super(CodeTag.R);
		sign = null;
		term = null;
		r = null;
	}
	
	@Override
	public boolean isEmpty() {
		if (sign == null && term == null && r == null)
			return true;
		return false;
	}
	
	@Override
	public void cleanUpNullVals() {
		if (r.isEmpty()) {
			r = null;
		} else {
			r.cleanUpNullVals();
		}
		term.cleanUpNullVals();
	}
	
	@Override
	public void traverseTree(){
		term.traverseTree();
		
		if (sign != null) {
			super.printCode(GrammarTree.signMap.get(sign.getTag()));
		}
		
		if (r != null){
			r.traverseTree();
		}
	}
}
