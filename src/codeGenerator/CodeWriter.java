package codeGenerator;

import interCode.GrammarTree;
import interCode.Prog;

public class CodeWriter {
	private GrammarTree code;
	
	public CodeWriter(Prog prog, String fileName) {
		code = new GrammarTree(0);
		code.assignProg(prog);
		code.createOutputFile(fileName);
	}

	
	public void write() {
		code.cleanUpNullVals();
		code.createCode();
	}
	
	
}
