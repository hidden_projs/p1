package defaultHashMap;

import java.util.HashMap;

import tokens.Ident;

public class DefaultHashMap<K, V> extends HashMap<K, V> {
	protected Ident defaultValue;

	public void setDefault(Ident defaultValue) {
		this.defaultValue = defaultValue;
	}

	@Override
	public V get(Object k) {
		if (containsKey(k)) 
			return super.get(k);
		if(defaultValue != null)
			return (V) defaultValue;
		return null;
	}

	public void unSetDefault() {
		this.defaultValue = null;
	}
}
