package batoMiniCompiler;

import interCode.Prog;

import java.util.Stack;

import codeGenerator.CodeWriter;

import parser.Parser;
import lexer.Lexer;
import lexer.SemanticException;

public class Main {

	public static void main(String[] args) throws SemanticException {
		InputCodeReader logoReader = new InputCodeReader("hilbert.t");
		Lexer lex = new Lexer(logoReader.getLogoCode());
		lex.createTokensFromString();
		Parser par = new Parser(lex.getTokenList());
		par.parse();
		Prog programm = par.getParsedProg();
		CodeWriter codeWriter = new CodeWriter(programm, "programm.txt");
		codeWriter.write();
	}

}
