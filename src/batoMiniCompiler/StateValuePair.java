package batoMiniCompiler;

public class StateValuePair {
	private int currState;
	private String transition;
	
	public StateValuePair(int currState, String transition) {
		this.currState = currState;
		this.transition = transition;
	}

	public int getCurrState() {
		return currState;
	}

	public String getTransition() {
		return transition;
	}
	
	
}
