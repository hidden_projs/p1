package batoMiniCompiler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InputCodeReader {
	BufferedReader consoleReader = null;
	BufferedReader fileReader = null;
	String fileNameIn = "";
	File file;
	String userInput;
	boolean exitApp = false;

	public InputCodeReader(String filename) {
		fileNameIn = filename;
	}


	public String getLogoCode() {
		String returnString = "";
		String line;
		file = new File(fileNameIn);
		if (file.length() != 0) {
			try {
				fileReader = new BufferedReader(new FileReader(fileNameIn));

				while ((line = fileReader.readLine()) != null) {
					returnString += (line + "\n");
				}

			} catch (FileNotFoundException e) {
				System.out.println("Input file not found");
				System.out.println();
			} catch (IOException e) {
				System.out.println("IO EXCEPTION");
				System.out.println(e.getMessage());
				System.out.println();
			} finally {
				if (fileReader != null) {
					try {
						fileReader.close();
					} catch (IOException e) {
						System.out.println("IO EXCEPTION 2");
						System.out.println(e.getMessage());
						System.out.println();
					}
				}
			}
		} else {
			System.out.println("File Is Empty!");
		}
		return returnString;
	}

}
