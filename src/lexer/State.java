package lexer;

import java.util.HashMap;

import defaultHashMap.DefaultHashMap;

import tokens.Ident;
import tokens.Num;
import tokens.Tag;
import tokens.Token;

public class State {
	private int id = 0;
	private String currString = "";
	private int lineNumber;
	private DefaultHashMap<String, Token> reservedWords = new DefaultHashMap<String, Token>();
	private HashMap<Integer, Token> retToken = new HashMap<Integer, Token>();

	public State(int id) {
		this.id = id;

		for (int i = 0; i < 20; i++) {
			retToken.put(i, null);
		}

		updateHashMaps();
	}

	private void updateHashMaps() {
		reservedWords.put("PROC", new Ident("PROC", Tag.PROC, lineNumber));
		reservedWords.put("FORWARD", new Ident("FORWARD", Tag.FORWARD, lineNumber));
		reservedWords.put("LEFT", new Ident("LEFT", Tag.LEFT, lineNumber));
		reservedWords.put("RIGHT", new Ident("RIGHT", Tag.RIGHT, lineNumber));
		reservedWords.put("IF", new Ident("IF", Tag.IF, lineNumber));
		reservedWords.put("THEN", new Ident("THEN", Tag.THEN, lineNumber));
		reservedWords.put("ELSE", new Ident("ELSE", Tag.ELSE, lineNumber));
		reservedWords.put("ENDIF", new Ident("ENDIF", Tag.ENDIF, lineNumber));
		
		retToken.put(20, new Token(Tag.PLUS, lineNumber));
		retToken.put(21, new Token(Tag.MINUS, lineNumber));
		retToken.put(22, new Token(Tag.TIMES, lineNumber));
		retToken.put(23, new Token(Tag.DIVIDE, lineNumber));
		retToken.put(24, new Token(Tag.LT, lineNumber));
		retToken.put(25, new Token(Tag.GT, lineNumber));
		retToken.put(26, new Token(Tag.LP, lineNumber));
		retToken.put(27, new Token(Tag.RP, lineNumber));
		// CHANGE
		retToken.put(28, reservedWords.get(currString));
		// CHANGE
		retToken.put(30, new Token(Tag.LE, lineNumber));
		retToken.put(31, new Token(Tag.EQ, lineNumber));
		retToken.put(32, new Token(Tag.GE, lineNumber));
		retToken.put(33, new Token(Tag.GE, lineNumber));
		
		if (id == 19) {
			retToken.put(19, new Num(Integer.valueOf(currString), lineNumber));
		} else if (id == 28) {
			reservedWords.unSetDefault();			
			if (reservedWords.get(currString) == null) {
				reservedWords.setDefault(new Ident(currString, Tag.IDENT, lineNumber));
			}
			retToken.put(28, reservedWords.get(currString));
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isAccepting() {
		if (this.id >= 0 && this.id <= 18)
			return false;
		return true;
	}

	public Token getToken(String buffString, int lineCounter) {
		lineNumber = lineCounter;
		currString = buffString;
		updateHashMaps();
		
		
		return retToken.get(id);
	}
}
