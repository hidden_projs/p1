package lexer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tokens.Token;

public class Lexer {
	private String codeString;
	private String terminate = "eof";
	private ArrayList<Token> tokens = new ArrayList<Token>();
	private int lineCounter = 1;

	private HashMap<Integer, HashMap<String, Integer>> stateTransition;
	private HashMap<String, Integer> transValNewState;

	public Lexer(String codeString) {
		this.codeString = codeString + " " + terminate + " ";
		LexerDFA lexFDA = new LexerDFA();
		stateTransition = new HashMap(lexFDA.getDFA());
	}

	public void createTokensFromString() throws SemanticException {
		State currState = new State(0);
		int lexemeBegin = 0;
		int lastChar = 0;
		String buffString = "";
		String nextChar = "";

		while (!(buffString.equals(terminate))) {
			
			if (currState.isAccepting()) {
				// last char is currently taking a peek on next character to
				// define current one, subtract one to get to the actual end of
				// lexeme
				lastChar--;
				lexemeBegin = lastChar;
				// white space accepting state = 29
				if (currState.getId() != 29) {
					tokens.add(currState.getToken(buffString, lineCounter));
				} else if (buffString.contains("\n")){
					lineCounter++;
				}

				currState.setId(0);
			} else {
				// setting buffString to value before peeking at next char
				buffString = codeString.substring(lexemeBegin, lastChar);
				nextChar = codeString.substring(lastChar, lastChar + 1);
				currState.setId(checkIfTransitionPossible(nextChar,
						currState.getId(), buffString));

				lastChar++;
			}
		}
		System.out.println("SUCCESS LEXING");
		System.out.println();
		System.out.println();
		System.out.println();
		
	}

	private int checkIfTransitionPossible(String nextChar, int stateNum,
			String currBuffer) throws SemanticException {
		int returnState = -1;

		transValNewState = new HashMap<String, Integer>(
				stateTransition.get(stateNum));
		for (Map.Entry<String, Integer> entry : transValNewState.entrySet()) {
			if (nextChar.matches(entry.getKey().toString())) {
				returnState = entry.getValue();
			}
		}
		if (returnState == -1) {
			String report = "";
			for (Map.Entry<String, Integer> entry : transValNewState.entrySet()) {
				report += "\t" + entry.getKey() + "\n";
			}
			report = "\nerror lexing, unexpected char: " + nextChar + 
					"\nincorrect string: " + currBuffer + nextChar + " at line: " + lineCounter + "\nexpected to match against: \n"
					+ report;
			throw new SemanticException(report);
		}
		return returnState;
	}
	
	public ArrayList<Token> getTokenList() {
		return tokens;
	}

}