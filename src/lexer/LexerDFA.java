package lexer;

import java.util.HashMap;

public class LexerDFA {
	
	private HashMap<Integer, HashMap<String, Integer>> stateTransition = new HashMap<Integer, HashMap<String, Integer>>();
	private HashMap<String, Integer> transValNewState = new HashMap<String, Integer>();
	
	public LexerDFA () {
		
		for (int i = 0; i < 30; i++) {
			stateTransition.put(i, null);
		}
		
		transValNewState.put("0", 2);
		transValNewState.put("[1-9]", 3);
		transValNewState.put("[a-zA-z]", 4);
		transValNewState.put("\\s", 5);
		transValNewState.put("<", 6);
		transValNewState.put(">", 7);
		transValNewState.put("=", 8);
		transValNewState.put("!", 10);
		transValNewState.put("\\+", 9);
		transValNewState.put("-", 11);
		transValNewState.put("\\*", 1);
		transValNewState.put("\\/", 12);
		transValNewState.put("\\(", 13);
		transValNewState.put("\\)", 14);
		stateTransition.put(0, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[0-9a-zA-Z\\s\\(]", 22);
		stateTransition.put(1, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[\\s<>=!\\-\\+\\*\\/\\(\\)]", 19);
		stateTransition.put(2, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();
		// [ws] [<] [>] [=] [!] [+] [-] [*] [/] [(] [)] 19
		transValNewState.put("[\\s<>=!\\-\\+\\*\\/\\(\\)]", 19);
		transValNewState.put("[0-9]", 3);
		stateTransition.put(3, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[0-9a-zA-Z]", 4);
		transValNewState.put("[\\s<>=!\\-\\+\\*\\/\\(\\)]", 28);
		stateTransition.put(4, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[0-9a-zA-Z<>=!\\-\\+\\*\\/\\(\\)]", 29);
		transValNewState.put("[\\s]", 5);
		stateTransition.put(5, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[0-9a-zA-Z\\s\\(]", 24);
		transValNewState.put("=", 17);
		stateTransition.put(6, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[0-9a-zA-Z\\s\\(]", 25);
		transValNewState.put("=", 18);
		stateTransition.put(7, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[=]", 15);
		stateTransition.put(8, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[0-9a-zA-Z\\s\\(]", 20);
		stateTransition.put(9, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[=]", 16);
		stateTransition.put(10, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[0-9a-zA-Z\\s\\(]", 21);
		stateTransition.put(11, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[0-9a-zA-Z\\s\\(]", 23);
		stateTransition.put(12, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[0-9a-zA-Z\\s\\(]", 26);
		stateTransition.put(13, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[0-9a-zA-Z\\s<>=!\\-\\+\\*\\/\\(\\)]", 27);
		stateTransition.put(14, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[0-9a-zA-Z\\s\\(]", 31);
		stateTransition.put(15, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[0-9a-zA-Z\\s\\(]", 33);
		stateTransition.put(16, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[0-9a-zA-Z\\s\\(]", 30);
		stateTransition.put(17, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();

		transValNewState.put("[0-9a-zA-Z\\s\\(]", 32);
		stateTransition.put(18, new HashMap<String, Integer>(transValNewState));
		transValNewState.clear();
	}
	
	public  HashMap<Integer, HashMap<String, Integer>> getDFA() {
		return stateTransition;
	}
}
